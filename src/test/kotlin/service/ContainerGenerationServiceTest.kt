package service

import entities.Image
import org.junit.Test

import org.junit.Before
import org.mockito.Matchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import utils.ResourceAgent

class ContainerGenerationServiceTest {

    @Mock
    private lateinit var resourceAgent: ResourceAgent

    @Mock
    private lateinit var assetGenerationService: AssetGenerationService

    private lateinit var underTest: ContainerGenerationService

    private val asset = Image(
        id=10L,
        name="0",
        url="https://google.com",
        expirationDate = "11/11/2011"
    )

    private val assetList = listOf(asset, asset, asset)

    private val containerList1 = listOf("10","potato","description")

    private val containerList2 = listOf("11", "lemon", "not%20a%20description")

    private val containerList3 = listOf("but I'm not a rapper", "lemon", "not%20a%20description")

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        underTest = ContainerGenerationService(this.resourceAgent,this.assetGenerationService)
        `when`(assetGenerationService.generateAssets(Matchers.anyString())).thenReturn(assetList)
    }

    @Test
    fun generateOneContainer() {
        `when`(resourceAgent.getContainerCsv()).thenReturn(listOf(containerList1))
        val containerList = this.underTest.generateContainers()
        assert(containerList.size == 1)
    }

    @Test
    fun generateMultipleContainers() {
        `when`(resourceAgent.getContainerCsv()).thenReturn(listOf(containerList1,containerList2))
        val containerList = this.underTest.generateContainers()
        assert(containerList.size == 2)
    }

    @Test
    fun generateNoContainers() {
        `when`(resourceAgent.getContainerCsv()).thenReturn(listOf())
        val containerList = this.underTest.generateContainers()
        assert(containerList.isEmpty())
    }

    @Test
    fun generateNoContainersDueToNumberFormatException() {
        `when`(resourceAgent.getContainerCsv()).thenReturn(listOf(containerList3))
        val containerList = this.underTest.generateContainers()
        assert(containerList.isEmpty())
    }
}