package service

import constants.AssetTypeIndicator
import constants.VideoTypeIndicator
import entities.Video
import factory.AssetFactory
import org.junit.Test

import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import utils.ResourceAgent

class AssetGenerationServiceTest {

    @Mock
    private lateinit var resourceAgent: ResourceAgent

    @Mock
    private lateinit var assetFactory: AssetFactory

    private var mockAssetList = mutableListOf("","","","","","Psych")

    private val mockAsset = Video(
        VideoTypeIndicator.CLIP,
        10L,
        "Potato",
        AssetTypeIndicator.VIDEO,
        "https://potato.com",
        "10/18/1980"
    )

    private lateinit var underTest: AssetGenerationService

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        this.underTest =  AssetGenerationService(this.resourceAgent, this.assetFactory)
    }

    @Test
    fun generateAssetsWithValidAssetTypeIndicator() {
        this.mockAssetList = mutableListOf("","","","","VIDEO","Psych")
        `when`(this.resourceAgent.getAssetCsv()).thenReturn(listOf(this.mockAssetList))
        `when`(this.assetFactory.getAsset(AssetTypeIndicator.VIDEO, this.mockAssetList))
            .thenReturn(this.mockAsset)
        val assetList = this.underTest.generateAssets("Psych")
        assert(assetList.size == 1)
    }

    @Test
    fun generateAssetsWithInvalidAssetTypeIndicator() {
        this.mockAssetList = mutableListOf("","","","","","Psych")
        `when`(this.resourceAgent.getAssetCsv()).thenReturn(listOf(this.mockAssetList))
        `when`(this.assetFactory.getAsset(AssetTypeIndicator.VIDEO, this.mockAssetList))
            .thenReturn(this.mockAsset)
        val assetList = this.underTest.generateAssets("Psych")
        assert(assetList.isEmpty())
    }
}