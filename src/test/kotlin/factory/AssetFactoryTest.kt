package factory

import constants.AssetTypeIndicator
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import utils.ResourceAgent
import java.lang.IllegalArgumentException

class AssetFactoryTest {

    @Mock
    private val resourceAgent: ResourceAgent = Mockito.mock(ResourceAgent::class.java)

    private lateinit var underTest: AssetFactory

    private val assetDetailsMap = mutableMapOf(10L to "5")

    private val advertisementAssetList = listOf("10","potato","https://taco.com","10/18/1991","advertisement","show")

    private val videoAssetList = listOf("10","potato","https://taco.com","10/18/1991","video","show")

    private val imageAssetList = listOf("10","potato","https://taco.com","10/18/1991","image","show")


    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        this.underTest = AssetFactory(this.resourceAgent)
        `when`(this.resourceAgent.getAssetDetailsMap()).thenReturn(this.assetDetailsMap)
    }

    @Test
    fun advertisementTest() {
        val testAsset = this.underTest.getAsset(AssetTypeIndicator.ADVERTISEMENT, advertisementAssetList)
        assert(testAsset.javaClass.simpleName == "Advertisement")
    }

    @Test
    fun imageTest() {
        val testAsset = this.underTest.getAsset(AssetTypeIndicator.IMAGE, imageAssetList)
        assert(testAsset.javaClass.simpleName == "Image")
    }

    @Test(expected = IllegalArgumentException::class)
    fun illegalArgumentVideoTest() {
        this.assetDetailsMap[10L] = "Potato"
        val testAsset = this.underTest.getAsset(AssetTypeIndicator.VIDEO, videoAssetList)
        assert(testAsset.javaClass.simpleName == "Video")
    }

    @Test()
    fun videoTest() {
        this.assetDetailsMap[10L] = "movie"
        val testAsset = this.underTest.getAsset(AssetTypeIndicator.VIDEO, videoAssetList)
        assert(testAsset.javaClass.simpleName == "Video")
    }

}