package entities

import constants.AssetTypeIndicator

class Advertisement(
    val productDescription: String,
    override val id: Long,
    override val name: String,
    override val typeIndicator: AssetTypeIndicator = AssetTypeIndicator.ADVERTISEMENT,
    override val url: String,
    override val expirationDate: String
) :
    Asset(id, name, typeIndicator, url, expirationDate) {

    override fun toString(): String {
        return super.toString() + "\tProduct Description: ${this.productDescription}\n"
    }

}