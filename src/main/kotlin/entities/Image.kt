package entities

import constants.AssetTypeIndicator

class Image(
    override val id: Long,
    override val name: String,
    override val typeIndicator: AssetTypeIndicator = AssetTypeIndicator.IMAGE,
    override val url: String,
    override val expirationDate: String
) :
    Asset(id, name, typeIndicator, url, expirationDate)