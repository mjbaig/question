package entities

import constants.AssetTypeIndicator
import constants.VideoTypeIndicator

class Video(
    val videoTypeIndicator: VideoTypeIndicator,
    override val id: Long,
    override val name: String,
    override val typeIndicator: AssetTypeIndicator = AssetTypeIndicator.VIDEO,
    override val url: String,
    override val expirationDate: String
) : Asset(id, name, typeIndicator, url, expirationDate) {

    override fun toString(): String {
        return super.toString() + "Video Type: ${this.videoTypeIndicator}\n"
    }
}