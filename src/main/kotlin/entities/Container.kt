package entities

class Container(
    val id: Long,
    val name: String,
    val assetList: List<Asset>,
    val description: String
) {
    override fun toString(): String {
        val assetString = this.assetList.fold("\n") { joiningValue, asset2 ->
            joiningValue + asset2.toString() + "\n"
        }
        return "Container Name: ${this.name}\n" +
                "\tContainer ID: ${this.id}\n" +
                "\tDescription: ${this.description}\n" +
                "----------------------------------" + assetString
    }
}