package entities

import constants.AssetTypeIndicator

abstract class Asset(
    open val id: Long,
    open val name: String,
    open val typeIndicator: AssetTypeIndicator,
    open val url: String,
    open val expirationDate: String
) {
    override fun toString(): String {
        return "Asset Type ${this.typeIndicator}\n" +
                "\tID: ${this.id}\n" +
                "\tName: ${this.name}\n" +
                "\tURL: ${this.url}\n" +
                "\tExpiration Date: ${this.expirationDate}\n"
    }
}