package utils

import com.google.common.io.Resources

open class ResourceAgent {

    private val assetsCsv: List<List<String>> = Resources.getResource("assets.csv").readText().split('\n')
        .filter {
            !it.isEmpty() && it.split(',').size == 6
        }
        .map { line ->
            line.split(',')
        }

    private val assetDetailsMap =
        Resources.getResource("asset_details.csv").readText().split('\n')
            .filter {
                !it.isEmpty()
            }
            .mapNotNull { line ->
                try {
                    val id = line.split(',')[0].toLong()

                    val details = line.split(',')[1]
                    mapOf(id to details)
                } catch (e: NumberFormatException) {
                    null
                }
            }.reduceRight { map1, map2 ->
                map1 + map2
            }

    private val containerCsv: List<List<String>> = Resources.getResource("container.csv").readText().split('\n')
        .filter {
            !it.isEmpty() && it.split(',').size == 3
        }
        .map { line ->
            line.split(',')
        }

    open fun getAssetCsv(): List<List<String>> {
        return this.assetsCsv
    }

    open fun getAssetDetailsMap(): Map<Long, String> {
        return this.assetDetailsMap
    }

    open fun getContainerCsv(): List<List<String>> {
        return this.containerCsv
    }
}