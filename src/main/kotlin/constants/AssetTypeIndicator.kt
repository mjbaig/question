package constants

enum class AssetTypeIndicator {
    VIDEO,
    IMAGE,
    ADVERTISEMENT
}