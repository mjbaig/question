package constants

enum class VideoTypeIndicator {
    MOVIE,
    FULL_EPISODE,
    CLIP
}