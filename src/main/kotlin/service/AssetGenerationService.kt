package service

import constants.AssetTypeIndicator
import entities.Asset
import factory.AssetFactory
import utils.ResourceAgent

open class AssetGenerationService(resourceAgent: ResourceAgent, assetFactory: AssetFactory) {

    private val resourceAgent = resourceAgent

    private val assetFactory = assetFactory

    open fun generateAssets(show: String): List<Asset> {
        return this.resourceAgent.getAssetCsv()
            .filter { asset ->
                asset[5] == show
            }
            .mapNotNull { asset ->
                try {

                    val typeIndicator = AssetTypeIndicator.valueOf(asset[4].toUpperCase())

                    this.assetFactory.getAsset(typeIndicator, asset)

                } catch (e: NumberFormatException) {
                    null
                } catch (e: IllegalArgumentException) {
                    null
                }
            }
    }
}