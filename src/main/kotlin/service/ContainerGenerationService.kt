package service

import entities.Container
import utils.ResourceAgent
import java.net.URLDecoder

class ContainerGenerationService(resourceAgent: ResourceAgent, assetGenerationService: AssetGenerationService) {

    private val resourceAgent = resourceAgent

    private val assetGenerationService = assetGenerationService

    fun generateContainers(): List<Container> {
        return this.resourceAgent.getContainerCsv().mapNotNull { container ->
            try {
                val id = container[0]
                val name = container[1]
                val description = container[2]
                Container(
                    id = id.toLong(),
                    name = URLDecoder.decode(name),
                    assetList = this.assetGenerationService.generateAssets(name),
                    description = URLDecoder.decode(description)
                )
            } catch (e: NumberFormatException) {
                null
            }
        }
    }


}

