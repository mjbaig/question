package factory

import constants.AssetTypeIndicator
import constants.VideoTypeIndicator
import entities.Advertisement
import entities.Asset
import entities.Image
import entities.Video
import utils.ResourceAgent
import java.net.URLDecoder

open class AssetFactory(resourceAgent: ResourceAgent) {

    private val resourceAgent = resourceAgent

    open fun getAsset(typeIndicator: AssetTypeIndicator, asset: List<String>): Asset {
        val id = asset[0].toLong()
        val name = URLDecoder.decode(asset[1])
        val url = URLDecoder.decode(asset[2])
        val expirationDate = asset[3]

        return when (typeIndicator) {
            AssetTypeIndicator.ADVERTISEMENT ->
                Advertisement(
                    id = id,
                    name = name,
                    url = url,
                    expirationDate = expirationDate,
                    productDescription = URLDecoder.decode(this.resourceAgent.getAssetDetailsMap()[id] ?: "")
                )
            AssetTypeIndicator.VIDEO -> {
                val videoTypeIndicator = URLDecoder.decode(this.resourceAgent.getAssetDetailsMap()[id] ?: "")

                Video(
                    id = id,
                    name = name,
                    url = url,
                    expirationDate = expirationDate,
                    videoTypeIndicator = VideoTypeIndicator.valueOf(videoTypeIndicator.toUpperCase())
                )
            }
            AssetTypeIndicator.IMAGE ->
                Image(
                    id = id,
                    name = name,
                    url = url,
                    expirationDate = expirationDate
                )
        }
    }

}