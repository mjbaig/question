import factory.AssetFactory
import service.AssetGenerationService
import service.ContainerGenerationService
import utils.ResourceAgent

fun main(args: Array<String>) {

    val resourceAgent = ResourceAgent()

    val assetFactory = AssetFactory(resourceAgent)

    val assetGenerationService = AssetGenerationService(resourceAgent, assetFactory)

    val containerGenerationService = ContainerGenerationService(resourceAgent, assetGenerationService)

    val containerList = containerGenerationService.generateContainers()

    containerList.forEach {
        print(it.toString())
    }
}

